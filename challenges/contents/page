With the surge in artificial intelligence in applications catering to both business and
consumer needs, deep learning is more important than ever for meeting current and future
market demands. With this book, you'll explore deep learning, and learn how to put
machine learning to use in your projects. This second edition of Python Deep Learning will get you up to speed with deep learning,
deep neural networks, and how to train them with high-performance algorithms and
popular Python frameworks. You'll uncover different neural network architectures, such as
convolutional networks, recurrent neural networks, long short-term memory (LSTM)
networks, and capsule networks. You'll also learn how to solve problems in the fields of
computer vision, natural language processing (NLP), and speech recognition. You'll study
generative model approaches such as variational autoencoders and Generative Adversarial
Networks (GANs) to generate images. As you delve into newly evolved areas of
reinforcement learning, you'll gain an understanding of state-of-the-art algorithms that are
the main components behind popular game Go, Atari, and Dota. By the end of the book, you will be well-versed with the theory of deep learning along with
its real-world applications. This book is for data science practitioners, machine learning engineers, and those interested
in deep learning who have a basic foundation in machine learning and some Python
programming experience. A background in mathematics and conceptual understanding of
calculus and statistics will help you gain maximum benefit from this book.
What this book covers
Chapter 1, Machine Learning – an Introduction, will introduce you to the basic ML concepts
and terms that we'll be using throughout the book. It will give an overview of the most
popular ML algorithms and applications today. It will also introduce the DL library that
we'll use throughout the book.
Chapter 2, Neural Networks, will introduce you to the mathematics of neural networks.
We'll learn about their structure, how they make predictions (that's the feedforward part),
and how to train them using gradient descent and backpropagation (explained through
derivatives). The chapter will also discuss how to represent operations with neural
networks as vector operations.
Chapter 3, Deep Learning Fundamentals, will explain the rationale behind using deep neural
networks (as opposed to shallow ones). It will take an overview of the most popular DL
libraries and real-world applications of DL.
Chapter 4, Computer Vision with Convolutional Networks, teaches you about convolutional
neural networks (the most popular type of neural network for computer vision tasks). We'll
learn about their architecture and building blocks (the convolutional, pooling, and capsule
layers) and how to use a convolutional network for an image classification task.
Chapter 5, Advanced Computer Vision, will build on the previous chapter and cover more
advanced computer vision topics. You will learn not only how to classify images, but also
how to detect an object's location and segment every pixel of an image. We'll learn about
advanced convolutional network architectures and the useful practical technique of transfer
learning.
Chapter 6, Generating Images with GANs and VAEs, will introduce generative models (as
opposed to discriminative models, which is what we'll have covered up until this point).
You will learn about two of the most popular unsupervised generative model approaches,
VAEs and GANs, as well some of their exciting applications.
Chapter 7, Recurrent Neural Networks and Language Models, will introduce you to the most
popular recurrent network architectures: LSTM and gated recurrent unit (GRU). We'll
learn about the paradigms of NLP with recurrent neural networks and the latest algorithms
and architectures to solve NLP problems. We'll also learn the basics of speech-to-text
recognition.
Chapter 8, Reinforcement Learning Theory, will introduce you to the main paradigms and
terms of RL, a separate ML field. You will learn about the most important RL
algorithms. We'll also learn about the link between DL and RL. Throughout the chapter, we
will use toy examples to better demonstrate the concepts of RL.
Chapter 9, Deep Reinforcement Learning for Games, you will understand some real-world
applications of RL algorithms, such as playing board games and computer games. We'll
learn how to combine the knowledge from the previous parts of the book to create betterthan-human computer players on some popular games.
Chapter 10, Deep Learning in Autonomous vehicles, we'll discuss what sensors autonomous
vehicles use, so they can create the 3D model of the environment. These include cameras,
radar sensors, ultrasound sensors, Lidar, as well as accurate GPS positioning. We'll talk
about how to apply deep learning algorithms for processing the input of these sensors. For
example, we can use instance segmentation and object detection to detect pedestrians and
vehicles using the vehicle cameras. We'll also make an overview of some of the approaches
vehicle manufacturers use to solve this problem (for example Audi, Tesla, and so on).