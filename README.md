Introductory Python Programming course as taught in NSA's Comp3321.

Directory Structure
- lessons (all lesson notebooks)
  - img (image files for lessons)
- practice (some practice files)
