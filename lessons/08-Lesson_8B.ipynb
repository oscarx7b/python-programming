{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "# Exception Handling\n",
    "\n",
    "When exceptions might occur, the best course of action to is to **handle** them and do something more useful than exit and print something to the screen. In fact, sometimes exceptions can be very useful tools(e.g. `KeywordInterrupt`). In Python, we handle exceptions with the `try` and `except` commands.\n",
    "Here it works:\n",
    "1. Everything between the `try` and `except` commands is executed.\n",
    "2. If that produces no exception, the `except` block is skipped and the program continues.\n",
    "3. If an exception occurs, the rest of the `try` block is skipped.\n",
    "4. If the type of exception is named after the `except` keyword, the code after the except command is executed.\n",
    "5. Otherwise, the execution stops and you have an **unhandled exception**.\n",
    "\n",
    "Everything makes more sense with an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    try:\n",
    "        print(\"I am going to convert the input to an integer\")\n",
    "        print (int(x))\n",
    "    except ValueError:\n",
    "        print (\"Sorry, I was not able to convert that.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "f(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "f('2')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "f('two')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "You can add multiple Exception types to the except command:\n",
    "```python\n",
    "... except (TypeError, ValueError):\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "The keyword `as` lets us grab the message from the error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "def be_careful(a,b):\n",
    "    try:\n",
    "        print(float(a)/float(b))\n",
    "    except (ValueError, TypeError, ZeroDivisionError) as detail:\n",
    "        print(\"Handled Exception: \", detail)\n",
    "    except:\n",
    "        print(\"Unexpected error!\")\n",
    "    finally:\n",
    "        print(\"THIS WILL ALWAYS RUN!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "be_careful(1,0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "be_careful(1,[1,2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "be_careful(1,'two')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "be_careful(16**400,1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "float(16**400)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "We've also added the `finally` command. It will always be executed, regardless of whether there was an exception or not, so it should be used as a place to clean up anything left over from the `try` and `except` clauses, e.g. closing files that might still be open.\n",
    "\n",
    "## Raising Exceptions\n",
    "\n",
    "Sometimes, you will want to cause an exception and let someone else handle it. This can be done with the `raise` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "raise TypeError('You submitted the wrong type')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "If no built-in exception is suitable for what you want to raise, defining a new type of exception is as easy as creating a new class that inherits from the `Exception` type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "class MyPersonalError (Exception):\n",
    "    pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "raise MyPersonalError(\"I am mighty. Hear my roar!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "def locator (myLocation):\n",
    "    if (myLocation<0):\n",
    "        raise MyPersonalError(\"I am mighty. Hear my roar!\")\n",
    "    print(myLocation)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "locator(-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "When catching an exception and raising a different one, both exceptions will be raised (as of Python 3.3)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "class MyException(Exception):\n",
    "    pass\n",
    "\n",
    "try:\n",
    "    int(\"abc\")\n",
    "except ValueError:\n",
    "    raise MyException(\"You can't convert text to an integer!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "You can override this by adding the syntax `from None` to the end of your `raise` statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MyException(Exception):\n",
    "    pass\n",
    "\n",
    "try:\n",
    "    int(\"abc\")\n",
    "except ValueError:\n",
    "    raise MyException(\"You can't convert text to an integer!\") from None"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "notebook_metadata_filter": "-all",
   "text_representation": {
    "extension": ".md",
    "format_name": "markdown"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
