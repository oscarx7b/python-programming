{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "# Modules\n",
    "\n",
    "At its most basic, a **module** in Python is really just another name for a script. It's just a file containing Python definitions and statements. The filename is the module's name followed by a `.py` extension. Typically, though, we don't run modules directly -- we _import_ their definitions into our own code and use them there. Modules enable us to write _modular_ code by organizing our program into logical units and putting those units in separate files. We can then share and reuse those files individually as parts of other programs.\n",
    "\n",
    "## Standard Modules\n",
    "\n",
    "Python ships with a library of standard modules, so you can get pretty far without writing your own. We've seen some of these modules already, and much of next week will be devoted to learning more about useful ones. They are documented in full detail in the [Python Standard Library reference](https://docs.python.org/3.7/library/).\n",
    "\n",
    "## An awesome example\n",
    "\n",
    "To understand modules better, let's make our own. This will put some Python code in a file called `awesome.py` in the current directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "contents = '''\n",
    "class Awesome(object):\n",
    "    def __init__(self, awesome_thing):\n",
    "        self.thing = awesome_thing\n",
    "    def __str__(self):\n",
    "        return \"{0.thing} is awesome!!!\".format(self)\n",
    "\n",
    "a = Awesome(\"Everything\")\n",
    "print(a)\n",
    "'''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('awesome.py', 'w') as f:\n",
    "    f.write(contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "Now you can run `python awesome.py` on the command line as a Python script.\n",
    "\n",
    "## Using modules: `import`\n",
    "\n",
    "You can also import `awesome.py` here as a module"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import awesome"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that you leave out the file extension when you import it. Python knows to look for a file in your path called `awesome.py`.\n",
    "The first time you import the module, Python executes the code inside it. Any defined functions, classes, etc. will be available for use. But notice what happens when you try to import it again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import awesome"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's assumed that the other statements (e.g. variable assignments, print) are there to help _initialize_ the module. That's why the module is only run once. If you try to import the same module twice, Python will not re-run the code -- it will refer back to the already-imported version. This is helpful when you import multiple modules that in turn import the same module.\n",
    "\n",
    "However, what if the module changed since you last imported it and you really want to do want to re-import it?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "contents = '''\n",
    "class Awesome(object):\n",
    "    def __init__ (self, awesome_thing):\n",
    "        self.thing = awesome_thing\n",
    "    def __str__ (self):\n",
    "        return \"{0.thing} is awesome!!!\".format(self)\n",
    "\n",
    "def cool(group):\n",
    "    return \"Everything is cool when you're part of {0}\".format(group)\n",
    "\n",
    "a = Awesome(\"Everything\")\n",
    "print(a)\n",
    "'''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('awesome.py', 'w') as f:\n",
    "    f.write(contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can bring in the new version with the help of the `importlib` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "import importlib\n",
    "importlib.reload(awesome)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calling the module's code\n",
    "\n",
    "The main point of importing a module is so you can use its defined functions, classes, constants, etc. By default, we access things defined in the `awesome` module by prefixing them with the module's name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(awesome.Awesome(\"A Nobel prize\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "awesome.cool(\"a team\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(awesome.a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What if we get tired of writing `awesome` all the time? We have a few options.\n",
    "\n",
    "## Using modules: `import __ as __ `\n",
    "\n",
    "First, we can pick a nickname for the module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import awesome as awe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(awe.Awesome(\"A book of Greek antiquities\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "awesome.cool(\"the Python developer community\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(awe.a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using modules: `from __ import __ `\n",
    "\n",
    "Second, we can import specific things from the `awesome` module into the current _namespace_:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from awesome import cool"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cool(\"this class\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# will this work?\n",
    "print(Awesome(\"A piece of string\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# will this work?\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get everything: `from __ import *`\n",
    "\n",
    "Finally, if you really want to import _everything_ from the module into the current namespace, you can do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# BE CAREFUL\n",
    "from awesome import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can re-run the cells above and get them to work.\n",
    "Why might you need to be careful with this method?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# what if you had defined this prior to import?\n",
    "def cool():\n",
    "    return \"Something important is pretty cool\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cool()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get one thing and rename: `from __ import __ as __ `\n",
    "\n",
    "You can use both `from` and `as` if you need to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from awesome import cool as coolgroup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cool()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coolgroup(\"the A team\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tidying up with `__main__`\n",
    "\n",
    "Remember how it printed something back when we ran `import awesome`? We don't need that to print out every time we import the module.(And really aren't initializing anything important.) Fortunately, Python provides a way to distinguish between running a file as a script and importing it as a module by checking the special variable `__name__`. Let's change our module code again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "contents = '''\n",
    "class Awesome(object):\n",
    "    def __init__ (self, awesome_thing):\n",
    "        self.thing = awesome_thing\n",
    "    def __str__ (self):\n",
    "        return \"{0.thing} is awesome!!!\".format(self)\n",
    "\n",
    "def cool(group):\n",
    "    return \"Everything is cool when you're part of {0}\".format(group)\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    a = Awesome(\"Everything\")\n",
    "    print(a)\n",
    "'''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "with open('awesome.py', 'w') as f:\n",
    "    f.write(contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now if you run the module as a script from the command line, it will make and print an example of the `Awesome` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run awesome.py in commandline\n",
    "%run -i 'awesome.py'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But if you import it as a module, it won't — you will just get the class and function definition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import importlib\n",
    "importlib.reload(awesome)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The magic here is that `__name__` is the name of the current module. When you import a module, its `__name__` is the module name (e.g. awesome), like you would expect. But a running script (or notebook) also uses a special module at the top level called `__main__`:\\_\\_name\\_\\_\n",
    "\n",
    "So when you run a module directly as a script(e.g `python awesome.py`), its `__name__` is actually `__main__`, not the module name any longer.\n",
    "\n",
    "This is a common convention for writing a Python script: organize it so that its functions and classes can be imported cleanly, and put the ’’glue\"\n",
    "code or default behavior you want when the script is run directly under the `__name__` check. Sometimes developers will also put the code in a function called `main()`and call that instead, like so:\n",
    "\n",
    "```\n",
    "def main():\n",
    "    a = Awesome(\"Everything\")\n",
    "    print(a)\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    main()\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "notebook_metadata_filter": "-all",
   "text_representation": {
    "extension": ".md",
    "format_name": "markdown"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
