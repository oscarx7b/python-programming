{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "# Iterators\n",
    "\n",
    "To create your own iterable objects, suitable for use in `for` loops and list comprehensions, all you need to do is implement the right special methods for the class.The `__iter__` method should return the iterable object itself (almost always `self`), and the `__next__` method defines the values of the iterator.\n",
    "\n",
    "Let’s do an example, sticking with the theme previously introduced, of an iterator that returns numbers in order, except for multiples of the arguments used at construction time. We'll make sure that it terminates eventually by raising the `StopIteration` exception whenever it gets to `200`.  (This is a great example of an _exception_ in Python that is not uncommon: handling an event that is not unexpected, but requires termination; `for` loops and list comprehension _expect_ to get the `StopIteration` exception as a signal to stop processing.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "class NonFactorIterable(object):\n",
    "    def __init__(self, *args):\n",
    "        self.avoid_multiples = args\n",
    "        self.x = 0\n",
    "    def __next__ (self):\n",
    "        self.x += 1\n",
    "        while True:\n",
    "            if self.x > 200:\n",
    "                raise StopIteration\n",
    "            for y in self.avoid_multiples:\n",
    "                if self.x % y == 0:\n",
    "                    self.x += 1\n",
    "                    break\n",
    "            else:\n",
    "                return self.x\n",
    "    def __iter__(self):\n",
    "        return self"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "silent_fizz_buzz = NonFactorIterable(3, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "[x for x in silent_fizz_buzz]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "mostly_prime = NonFactorIterable(2, 3, 5, 7, 11, 13, 17, 19)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "partial_sum = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "for x in mostly_prime:\n",
    "    partial_sum += x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "partial_sum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "mostly_prime = NonFactorIterable(2, 3, 5, 7, 11, 13, 17, 19)\n",
    "print(sum(mostly_prime))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It may be seem strange that the `__iter__` method doesn't appear to do anything. This is because in some cases the **iterator** for an object should not be the same as the object itself. Covering such usage is beyond the scope of the course.\n",
    "\n",
    "There is another way of implementing a custom iterator: the `__getitem__` method. This allows you to use the square bracket `[]` notation for getting data out of the object. However, you still must remember to raise a `StopIteration` exception for it to work properly in for loops and list comprehensions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "## Another iterator example\n",
    "\n",
    "In the below example, we create an iterator that returns the squares of numbers. Note that in the `__next__` method, all we're doing is iterating our counter (`self.x`) and returning the square of that counter number, as long as the counter is not greater than the pre-defined limit(`self.limit`). The `while` loop in the previous example was specific to that use-case; we don't actually need to implement any looping at all in `__next__`, as that's simply the method called for each iteration through a loop on our iterator.\n",
    "\n",
    "Here we're also implementing the `__getitem__` method, which allows us to retrieve a value from the iterator at a certain index location. This one simply calls the iterator using `self.__next__` until it arrives at the desired index location, then returns that value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "class Squares(object ):\n",
    "    def __init__ (self, limit=200):\n",
    "        self.limit = limit\n",
    "        self.x = 0\n",
    "    def __next__(self):\n",
    "        self.x += 1\n",
    "        if self.x > self.limit:\n",
    "            raise StopIteration\n",
    "        return (self.x-1)**2\n",
    "\n",
    "    def __getitem__(self,idx):\n",
    "        # initialize counter to 0\n",
    "        self.x = 0\n",
    "        if not isinstance(idx, int):\n",
    "            raise Exception(\"Only integer index arguments are accepted!\")\n",
    "        while self.x < idx:\n",
    "            self.__next__()\n",
    "        return self.x**2\n",
    "\n",
    "    def __iter__(self):\n",
    "        return self"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "my_squares = Squares(limit=20)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "[x for x in my_squares]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "my_squares[5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# since we set a Limit of 20, we can't access an index Location higher than that\n",
    "my_squares[25]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Benefits of Custom Iterators\n",
    "\n",
    "1. Cleaner code\n",
    "2. Ability to work with infinite sequences\n",
    "3. Ability to use built-in functions like `sum` that work with iterables\n",
    "4. Possibility of saving memory (e.g `range`)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "notebook_metadata_filter": "-all",
   "text_representation": {
    "extension": ".md",
    "format_name": "markdown"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
